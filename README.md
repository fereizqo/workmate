# WORKMATE #

This app implementing a clock-in and out feature that our workers use when they start work and end work..

### Overview ###
Assume that a worker has already applied to a job, the employer has approved them and the worker arrives at the appropriate location and time to start work.


### Feature ###

1. Start Work – Worker opens the app and taps Clock In to start working.
2. Clocking In – In case the worker has made a mistake, they have 10 seconds to cancel
3. Working – Once they have been clocked in, they are considered to have started work.
They can tap Clock Out to stop working
4. Clocking Out – Similar to Clocking In, In case the worker has made a mistake, they
have 10 seconds to cancel
5. Finished Work – Now that the worker has successfully clocked in and out, they have
completed their job for the day and will be paid on

### Resource ###

* Cocoapods
* Alamofire
* SwiftyJSON
* [Design Asset](https://www.figma.com/file/GXth0NGx1gwyoEGa3gKVc8/Mobile-Take-Home-Exercise?node-id=)

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

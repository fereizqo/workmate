//
//  APIRequestTest.swift
//  WorkmateAppTests
//
//  Created by Fereizqo Sulaiman on 21/02/20.
//  Copyright © 2020 Fereizqo Sulaiman. All rights reserved.
//

import XCTest
import Alamofire
@testable import WorkmateApp

class APIRequestTest: XCTestCase {
    
    func testGetWorks() {
        let expect = XCTestExpectation(description: "Not nil")
        var work: Work?
        
        APIRequest.shared.getWorks { result, error in
            work = result
            expect.fulfill()
        }
        
        wait(for: [expect], timeout: 5)
        XCTAssertNotNil(work)
    }
    
    
}

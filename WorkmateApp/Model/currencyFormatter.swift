//
//  currencyFormatter.swift
//  WorkmateApp
//
//  Created by Fereizqo Sulaiman on 21/02/20.
//  Copyright © 2020 Fereizqo Sulaiman. All rights reserved.
//

import Foundation

var currencyFormatter: NumberFormatter = {
    let formatter = NumberFormatter()
    
    formatter.numberStyle = .currency
    formatter.locale = Locale(identifier: "id_ID")
    formatter.maximumFractionDigits = 0
    formatter.minimumFractionDigits = 0
    formatter.paddingPosition = .afterPrefix
    formatter.paddingCharacter = " "
    
    return formatter
}()

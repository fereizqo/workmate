//
//  Work.swift
//  WorkmateApp
//
//  Created by Fereizqo Sulaiman on 21/02/20.
//  Copyright © 2020 Fereizqo Sulaiman. All rights reserved.
//

import Foundation

struct Work {
    var status: String?
    let clientName: String?
    let positionName: String?
    let locationAddress: String?
    let wageAmount: String?
    let wageType: String?
    let managerName: String?
    let managerPhone: String?
    var clock_in_time: String?
    var clock_out_time: String?
    
    init(status: String? = nil, clientName: String? = nil, positionName: String? = nil, locationAddress: String? = nil, wageAmount: String? = nil, wageType: String? = nil, managerName: String? = nil, managerPhone: String? = nil, clock_in_time: String? = nil, clock_out_time: String? = nil) {
        self.status = status
        self.clientName = clientName
        self.positionName = positionName
        self.locationAddress = locationAddress
        self.wageAmount = wageAmount
        self.wageType = wageType
        self.managerName = managerName
        self.managerPhone = managerPhone
        self.clock_in_time = clock_in_time
        self.clock_out_time = clock_out_time
    }
}

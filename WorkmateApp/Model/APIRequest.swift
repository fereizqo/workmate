//
//  APIRequest.swift
//  WorkmateApp
//
//  Created by Fereizqo Sulaiman on 21/02/20.
//  Copyright © 2020 Fereizqo Sulaiman. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class APIRequest {
    static let shared = APIRequest()
    private init() {}
    
    // MARK: - Get Request: Work
    func getWorks(completionHandler: @escaping (Work, NSError?) -> Void) {
        getWorkRequest(completion: completionHandler)
    }
    
    func getWorkRequest(completion: @escaping (Work, NSError?) -> Void) {
        let url = "https://api.helpster.tech/v1/staff-requests/26074/"

        Alamofire.request(url, method: .get)
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    let listData = JSON(value)
                    let status = listData["status"].stringValue
                    let clientName = listData["client"]["name"].stringValue
                    let positionName = listData["position"]["name"].stringValue
                    let locationAddress =  listData["location"]["address"]["street_1"].stringValue
                    let wageAmount = listData["wage_amount"].stringValue
                    let wageType = listData["wage_type"].stringValue
                    let managerName = listData["manager"]["name"].stringValue
                    let managerPhone = listData["manager"]["phone"].stringValue
                    
                    let work = Work(status: status, clientName: clientName, positionName: positionName, locationAddress: locationAddress, wageAmount: wageAmount, wageType: wageType, managerName: managerName, managerPhone: managerPhone)
                    
                    completion(work, nil)
                case .failure(let error):
                    let work = Work()
                    completion(work, error as NSError)
                }
        }
    }
    
    
    // MARK: - Post Request: Clocking In
    
    func doClockingIn(completionHandler: @escaping (Work, NSError?) -> Void) {
        doClockingRequestIn(completion: completionHandler)
    }
    
    func doClockingRequestIn(completion: @escaping (Work, NSError?) -> Void) {
        let url = "https://api.helpster.tech/v1/staff-requests/26074/clock-in/"
        
        // Parameter
        let parameter: Parameters = [
            "latitude": "-6.2446691",
            "longitude": "106.8779625"
        ]
        
        // Basic authentication
        let user = "+6281313272005"
        let password = "alexander"
        let credentialData = "\(user):\(password)".data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!
        let base64Credentials = credentialData.base64EncodedString()
        
        // Header
        let header = ["Authorization": "Basic \(base64Credentials)",
            "Content-Type": "application/json"]
        
        // Do post request
        Alamofire.request(url, method: .post, parameters: parameter, encoding: JSONEncoding.default, headers: header)
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    
                    let listData = JSON(value)
                    var status = listData["status"].stringValue
                    let clock_in_time = listData["clock_in_time"].stringValue
                    let clock_out_time = listData["clock_out_time"].stringValue
                    
                    // If already clocked in
                    if listData["code"].stringValue == "already_clocked_in" {
                        status = "clocked_in"
                    }
                    
                    let work = Work(status: status, clock_in_time: clock_in_time, clock_out_time: clock_out_time)
                    
                    completion(work, nil)
                    
                case .failure(let error):
                    let work = Work()
                    completion(work, error as NSError)
                }
        }
    }
    
    // MARK: - Post Request: Clocking Out
    
    func doClockingOut(completionHandler: @escaping (Work, NSError?) -> Void) {
        doClockingOutRequest(completion: completionHandler)
    }
    
    func doClockingOutRequest(completion: @escaping (Work, NSError?) -> Void) {
        let url = "https://api.helpster.tech/v1/staff-requests/26074/clock-out/"
        
        // Parameter
        let parameter: Parameters = [
            "latitude": "-6.2446691",
            "longitude": "106.8779625"
        ]
        
        // Basic authentication
        let user = "+6281313272005"
        let password = "alexander"
        let credentialData = "\(user):\(password)".data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!
        let base64Credentials = credentialData.base64EncodedString()
        
        // Header
        let header = ["Authorization": "Basic \(base64Credentials)",
            "Content-Type": "application/json"]
        
        // Do post request
        Alamofire.request(url, method: .post, parameters: parameter, encoding: JSONEncoding.default, headers: header)
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    
                    let listData = JSON(value)
                    let status = listData["timesheet"]["status"].stringValue
                    let clock_in_time = listData["timesheet"]["clock_in_time"].stringValue
                    let clock_out_time = listData["timesheet"]["clock_out_time"].stringValue
                    
                    let work = Work(status: status, clock_in_time: clock_in_time, clock_out_time: clock_out_time)
                    
                    completion(work, nil)
                    
                case .failure(let error):
                    let work = Work()
                    completion(work, error as NSError)
                }
        }
    }
}

//
//  LoadingViewController.swift
//  WorkmateApp
//
//  Created by Fereizqo Sulaiman on 21/02/20.
//  Copyright © 2020 Fereizqo Sulaiman. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class LoadingViewController: UIViewController {

    @IBOutlet weak var clockingLabel: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var barLoadView: UIView!
    @IBOutlet weak var fullBarLoadView: UIView!
    
    var timer = Timer()
    var work: Work?
    var mainViewControllerDelegate: MainViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Make timer action
        timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(timerAction), userInfo: nil, repeats: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // Make loading animation
        UIView.animate(withDuration: 3) {
            self.barLoadView.frame = CGRect(x: 0, y: 0, width: self.fullBarLoadView.frame.width, height: self.fullBarLoadView.frame.height)
        }
        
        // Set clocking status
        guard let work = MainViewController.work else { return }
        if work.status == "clocked_in" {
            clockingLabel.text = "Clocking Out..."
        } else {
            clockingLabel.text = "Clocking In..."
        }
    }
    
    @objc func timerAction() {
        // When timer is done, do timer action
        doPost()
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func cancelButtonTapped(_ sender: UIButton) {
        // When cancel timer, cancel timer action
        timer.invalidate()
        self.dismiss(animated: true, completion: nil)
    }
}


extension LoadingViewController {
    func doPost() {
        // Checking whether has a value or not
        guard let work = MainViewController.work else { return }
        
        // Do clocking in
        if work.status == "in_progress" {
            APIRequest.shared.doClockingIn() { result, error in
                if error == nil {
                    MainViewController.work?.status = result.status
                    MainViewController.work?.clock_in_time = result.clock_in_time
                    self.mainViewControllerDelegate?.setLabel()
                } else {
                    print("error")
                }
            }
        }
            
        // Do clocking out
        else if work.status == "clocked_in" {
            APIRequest.shared.doClockingOut() { result, error in
                if error == nil {
                    MainViewController.work?.status = result.status
                    MainViewController.work?.clock_in_time = result.clock_in_time
                    MainViewController.work?.clock_out_time = result.clock_out_time
                    self.mainViewControllerDelegate?.setLabel()
                } else {
                    print("error")
                }
            }
        }

    }
}

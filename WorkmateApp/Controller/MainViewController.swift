//
//  MainViewController.swift
//  WorkmateApp
//
//  Created by Fereizqo Sulaiman on 20/02/20.
//  Copyright © 2020 Fereizqo Sulaiman. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

protocol MainViewControllerDelegate {
    func setLabel()
}

class MainViewController: UIViewController {

    // Outlet Label
    @IBOutlet weak var jobPositionLabel: UILabel!
    @IBOutlet weak var jobClientLabel: UILabel!
    @IBOutlet weak var jobWageAmountLabel: UILabel!
    @IBOutlet weak var jobWageTypeLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var managerNameLabel: UILabel!
    @IBOutlet weak var managerPhoneLabel: UILabel!
    @IBOutlet weak var clockInTimeLabel: UILabel!
    @IBOutlet weak var clockOutTimeLabel: UILabel!
    
    // Outlet other element
    @IBOutlet weak var outerCircleView: UIView!
    @IBOutlet weak var clockButton: UIButton!
    
    public static var work: Work?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Setup appearance
        outerCircleView.layer.cornerRadius = outerCircleView.frame.height/2
        clockButton.layer.cornerRadius = clockButton.frame.height/2
        clockButton.titleLabel?.adjustsFontSizeToFitWidth = true
        
        // Get data
        getData()
    }
    
    func getData() {
        // Do get request
        APIRequest.shared.getWorks { result, error in
            if error == nil {
                // Get the result
                MainViewController.self.work = result
                // Update label
                self.setLabel()
            } else {
                print("error")
            }
            // When it's done, remove spinner
            Spinner.shared.removeSpinner()
        }
    }

    @IBAction func clockButtonTapped(_ sender: UIButton) {
        // Go to detail contact screen
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoadingViewController") as! LoadingViewController
        vc.modalPresentationStyle = .fullScreen
        vc.mainViewControllerDelegate = self
        self.present(vc, animated: true, completion: nil)
    }
}

extension MainViewController: MainViewControllerDelegate {
    func setLabel() {
        // Show spinner
        Spinner.shared.showSpinner(onView: view)
        // Checking whether has a value or not
        guard let work = MainViewController.self.work else { return }
        
        // Update label appearances
        jobPositionLabel.text = work.positionName
        jobClientLabel.text = work.clientName
        locationLabel.text = work.locationAddress
        managerNameLabel.text = work.managerName
        managerPhoneLabel.text = work.managerPhone
        
        // wageAmount string into currency
        if let wageAmount = Int(work.wageAmount ?? "0") {
            let wageAmountNumber = NSNumber(value: wageAmount)
            jobWageAmountLabel.text = currencyFormatter.string(from: wageAmountNumber)
            
        }
        
        // wageType separated by _
        if let wageTypeArray = work.wageType?.components(separatedBy: "_") {
            jobWageTypeLabel.text = "\(wageTypeArray[0]) \(wageTypeArray[1])"
        }
        
        // Update view in every state
        print("status: \(String(describing: work.status))")
        // Date formatter
        let formatterDateAPI = DateFormatter()
        formatterDateAPI.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ"
        
        let formatterDateAM = DateFormatter()
        formatterDateAM.dateFormat = "hh:mm a"
        formatterDateAM.timeZone = TimeZone(abbreviation: "UTC+7")
        
        // In progress
        if work.status == "in_progress" {
            // Setup view
            clockButton.titleLabel?.text = "Clock in"
            outerCircleView.isHidden = false
            outerCircleView.isUserInteractionEnabled = true
        
        // Clocked in
        } else if work.status == "clocked_in" {
            // Setup view
            clockButton.titleLabel?.text = "Clock out"
            
            // Update clock in time
            guard let input = work.clock_in_time else { return }
            guard let date = formatterDateAPI.date(from: input) else { return }
            let time = formatterDateAM.string(from: date)
            clockInTimeLabel.text = time
        
        // Clocked out
        } else if work.status == "disputed" {
            // Setup view
            clockOutTimeLabel.text = work.clock_out_time
            outerCircleView.isHidden = true
            outerCircleView.isUserInteractionEnabled = false
            
            // Update clock in time
            guard let inputIn = work.clock_in_time else { return }
            guard let dateIn = formatterDateAPI.date(from: inputIn) else { return }
            let timeIn = formatterDateAM.string(from: dateIn)
            clockInTimeLabel.text = timeIn
            
            // Update clock out time
            guard let inputOut = work.clock_out_time else { return }
            guard let dateOut = formatterDateAPI.date(from: inputOut) else { return }
            let timeOut = formatterDateAM.string(from: dateOut)
            clockOutTimeLabel.text = timeOut
        }
        
        // When it's done, remove spinner
        Spinner.shared.removeSpinner()
    }
}
